import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private modalService: NgbModal, private router: Router) { }

  ngOnInit(): void {
  }

  openSm(content) {
    this.modalService.open(content, { size: 'sm' });
  }

  logout(){
    console.log("Sesion cerrada");
    this.router.navigateByUrl('/login');
    this.modalService.dismissAll();
  }

  getDate(){
    let fecha = new Date();
    let hoy = fecha.getDate();

    if(hoy == 24){
      document.getElementById("primerdia").style.display="none";
    }else if(hoy == 25){
      document.getElementById("segundodia").style.display="none";
    }
  }

}
